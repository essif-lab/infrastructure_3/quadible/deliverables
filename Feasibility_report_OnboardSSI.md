# Feasibility report

# Table of Contents
1. [Introduction](#1-introduction)
2. [Key Accomplishments](#2-key-accomplishments)
3. [Deviations and takeaways](#3-deviations-and-takeaways)
4. [Integrations and External Parties](#4-integrations-and-external-parties)
5. [Interoperability](#5-interoperability)
6. [Standardisations](#6-standardisation)
7. [Deliverables](#7-deliverables)
8. [Exploitation](#8-exploitation)
9. [Dissemination](#9-dissemination)
10. [Conclusion](#10-conclusion)


# 1. Introduction
Quadible is a cybersecurity startup with strong a focus on identity including authentication, identification and identity management including Self-Sovereign Identity. Through the OnboardSSI project, the team developed a solution to allow citizens to easily and securely onboard their existing identity to the SSI through an existing identity document. OnboardSSI leverage AI to remotely verify users’ identity, without human validator intervention, and create verifiable credentials. End-users are asked to perform a 3-step identity verification including scanning of proof-of-identity (e.g. passport), person cross-check through liveness detection, facial matching, and document validation, reaching a High Level of Assurance based on eIDAS regulation. Through a technology-specific wallet, OnboardSSI provides the appropriate abstraction for citizens to easily manage their credentials. Finally, Quadible’s AI continuous behavioural authentication, secures the credentials, by combining 14 different behavioural and biometric traits. 

# 2. Key Accomplishments

| Goal | Achievement against set goals |
|------|-------------------------------|
| Development of a Mobile SDK that allows SSI user onboarding based on a passport with High Level of Assurance (eIDAS)| The OnboardSSI SDK allows EU citizens to seamlessly onboard their existing identity to SSI, based on a passport document with High Level of Assurance. The SDK supports currently the onboarding of existing EU passports and allows easy extension with other identity documents. The SDK supports both Android and iOS platforms through the Kotlin Multiplatform software development kit.
| Development of a Backend component for Issuers to query | The OnboardSSI backend component allows the temporary storage and exposure of the identity information of an EU citizen, that is in the process of onboarding the existing identity. OpenAPI based specifications enable the communication of the backend component with the Mobile SDK but also with the Issuer, in order to retrieve and expose the identity information to the Issuer.
|  Mobile Wallet, with seamless user onboarding and continuous behavioural authentication | The OnboardSSI mobile wallet allows the onboarding of existing identity document and identity verification with High Level of Assurance (eIDAS). The mobile wallet leverages the SSI technology provided by Evernym, enabling the creation of Verifiable Credentials. The mobile wallet is protected through an AI-powered continuous behavioural authentication solution that combines information on how a user moves, how the device is used, biometrics and transactional patterns.
| Integration with the Evernym Issuer/Verifier | For the integration with Evernym, a backend component was developed that operates as a bridge between the OnboardSSI backend component and the Evernym Issuer. The bridge forwards the communication requests received at the backend component from the mobile wallet. In this way, it was possible to demonstrate the OnboardSSI solution allowing the creation of Verifiable Credentials from strongly verified document identity and the verification of the Verifiable Credentials through the Evernym Verifier.

# 3. Deviations and takeaways

In terms of the deviations, no major deviations took. In the demonstration plan, it was defined that the OnboardSSI and the BehavAuth solutions would be integrated into the mobile app of the Evernym SSI solution. The integration of the OnboardSSI and BehavAuth with the Evernym SSI was completed through a wallet that incorporates the Mobile SSI of Evernym, providing better flexibility to the developers that want to understand how to integrate the two solutions into an existing SSI implementation. The choice was made considering that the majority of the solutions implement a separate wallet in order to manage the Verifiable Credentials. For that reason, a complementary SSI wallet was developed and integrated with the OnboardSSI and the BehavAuth. An additional benefit is that developers could leverage the existing wallet app in order to build on top of their custom solution without re-inventing the wheel.

The actual integration of the OnboardSSI and BehavAuth is very simple and easy, reducing the time for the developers to integrate the solution into their apps. Following the fact that the OnboardSSI solution comprises of a mobile and a web component, a clear exploitation and business plan is generated similar to what Github has followed. Meaning that the solution is offered open source with the Apache 2.0 license. However, the actual monetisation part is created through offering the solution ready deployed on a cloud infrastructure as Software-as-a-Service, where developers can just generate the access keys and then can start to perform API calls through the OnboardSSI mobile SDK. Having a solid business plan in place is very important in order to ensure the sustainability of the solution and the continuous maintenance and further development of complementary components that could assist by generating additional value for the different organisations that deploy such solution.

# 4. Integrations and External Parties


### Evernym
Evernym is a leading organisation in the area of Self-Sovereign Identity that has been acquired by Avast, offering the whole technology stack in terms of SSI. The OnboardSSI has been integrated with the Evernym SSI technology stack to showcase how the integration takes place and to demonstrate the system as a whole.

### Ubitech
UBITECH has been established in Athens, Greece back in 2005, concentrated in the European market and acquiring several EC and national grants for novel R&D initiatives. UBITECH has extended its operations with targeted international activities through its affiliated companies, subsidiaries and offices in Limassol (Cyprus) and Buenos Aires (Argentina). The DOOR and OnboardSSI projects will be integrated in order to offer higher authentication security when performing proof presentation. 

### Animo
Animo Solutions offer Self-sovereign identity development and consultancy with an expertise in open source and Hyperledger Aries. Integration with the OnboardSSI will provide an additional security mechanism for the mobile SSI SDK offered by the company.

### Sphereon
Sphereon was one of the first companies to provide easy-to-use solutions to bridge the gap between today’s world of document-centric business processes and the new decentralized world with communities, eco systems and value chains. Through the eIDAS bridge development by the company, together with the OnboardSSI, an end-to-end secure seal is offered, with High Level of Assurance.

### Systems Integration Solutions
Systems Integration Solutions, LLC is a leader in the field of information technology technical solutions, saying that it meets the requirements of the ISO 9001 certificate for compliance with high-quality services. 	Through the combination of the SIS authorisation mechanism and the OnboardSSI identity verification, an easier and more seamless experience will be offered in logistics.

### IDEMIA
IDEMIA is a multinational technology company headquartered in Courbevoie, France. It provides identity-related security services, and sells facial recognition and other biometric identification products and software to private companies and governments. Considering that IDEMIA is working on a competitive solution to OnboardSSI, actions have already performed to standardise the interfaces exposed to Issuer, to create wider adoption of such solutions.

# 5. Interoperability
Interoperability is crucial for the wide adoption of the SSI, considering the different types of implementations of the SSI which should be compatible with each other. For that reason, OnboardSSI has paid particular attention to ensuring the interoperability of the solution with different types of SSI implementations. Towards this end, OnboardSSI has performed certain actions both in terms of the technical side but also the business/exploitation side. From the technical point of view, the system has been designed allowing the integration of a mobile SDK and the backend component, which expose OpenAPI based interfaces, enabling standardised and easy integration of the solution with any SSI implementation. Through research of existing competing solutions, a similar approach was followed with these existing solutions as well as the interface exposed by these solutions. From the business/exploitation side, the solution has been integrated with different relevant components and SSI implementations that are part of the eSSIF-Lab project as well as standardisation activities have been performed which are detailed in the next section.

### Evernym
OnboardSSI has been integrated and demonstrated together with the Evernym SSI implementation showcasing the whole flow including of the registration, issuing and verification phases. The integration of the solution with Evernym was performed through the Evernym mobile SSI SDK and the Verity component that operates as Issuer and Verifier. 

### Ubitech
The Quadible team are exploring with Ubitech to integrate the OnboardSSI and the DOOR solutions as well as the BehavAuth component. The purpose of the integration is to create a highly secure authentication mechanism for the user, that will generate verifiable credentials signed by characteristics of the device as well as the behavioural traits of the particular user. In essence, the verifiable credentials will be accessible only when the device characteristics and the confidence score of the user's behaviour, are correct.

### Animo
The Quadible team will integrate the OnboardSSI mobile component with the Animo mobile SDK implemented through the eSSIF-Lab project, in order to provide the functionality of onboarding existing identity document to the SSI through the Animo mobile SDK.

### Sphereon
The eIDAS brirdge will be integrated with the OnboardSSI in order to strengthen the security of the Verifiable Credentials generated by the eIDAS bridge by performing the identity verification of the user before the issuance phase. In this way, an end-to-end secure solution will be offered where both the user side and the Verifiable Credentials will be secured, in order to receive a seal with High Level of Assurance.


### Systems Integration Solutions
The Systems Integration Solution is creating an SSI based authorization for cross-border government and business representatives in logistics, where the OnboardSSI will strongly verify the identity of the users.


# 6. Standardisation
In order to perform standardisation activities, the OnboardSSI team had to identify other solution providers that operate in the same area towards finding a common ground on how such solutions will be exposed and integrated with different types of SSI implementations. A competition analysis allowed the team to create a list of competitors that are working on offering a similar solution that will allow end-users to seamlessly onboard their identity document to the SSI. The result of the competition analysis based on research and feedback from the eSSIF-Lab consortium, showed that IDEMIA and Evernym are currently working on such solution. The team has already established a collaboration with both organisations. Quadible is currently working together with IDEMIA on getting EU funding related to identity management and initial conversations have taken place in order to create a common framework that will expose the APIs, offering interoperability among the solutions. Regarding Evernym, Quadible is already working with them considering the example integration and implementation performed in the eSSIF-Lab project, while discussions have taken place regarding the actual implementation on their document onboarding solution in order to define a common exposure interface. 

# 7. Deliverables
This section provides the information and the links to the deliverables provided throughout the duration of the OnboardSSI project including the Reports and the Source code.

## 7.1 Reports
Following, the deliverables of the OnboardSSI project to the eSSIF-Lab are described including the different milestones of the project. Each of the milestones is mapped to the specific deliverable i.e. document, source code, while appropriate links are given for each deliverable.

| Milestone | Date | Deliverables|
|-----------|------|-------------|
| Milestone 1 | 21 January 2022 | - [Functional Specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/deliverables/-/blob/master/functional_specification_of_OnboardSSI_component.md) <br /> - [Interface Specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/deliverables/-/blob/master/interface_specification_of_OnboardSSI_component.md) <br /> - [Envisioned Interoperability](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/deliverables/-/blob/master/envisioned_interoperability_with_others.md)
| Milestone 2 | 21 August 2022 | - [Feasibility Report](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/deliverables/-/blob/master/Feasibility%20Report%20OnboardSSI.md) <br />- [Source code](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/OnboardSSI/-/tree/master)

## 7.2 Source code
This subsection provides the information related to the source code implemented through the OnboardSSI project. The source code is released under the Apache 2.0 license, allowing even the commercial exploitation of the solution. Each of the software components include appropriate documentation that enable developers to deploy and even further develop the solutions. The source code includes various components initially for the OnboardSSI solution itself incorporating a wallet that deploys the actual OnboardSSI mobile SDK. Furthermore, to showcase and demonstrate the OnboardSSI solution as well as to provide an example on how to integrate the solution, an implementation of the OnboardSSI with the Evernym SSI stack is provided. Quadible will continue to maintain and develop the solution.

| Repository | Description |
|------------|-------------|
| [OnboardSSI API Specs](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/OnboardSSI/-/tree/master/OnboardSSI-API-Spec) | Open API Specification for the OnboardSSI backend component that defines the interfaces exposed by the component, so that the Issuer can query the information of the onboarded document and person.
| [OnboardSSI API](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/OnboardSSI/-/blob/master/OnboardSSI-API) | The backend implementation of the OnboardSSI that retrieves the verified information of the end-user, temporary stores the data and then exposes the information to the Issuer in order to cross-check the information with the national databases. |
| [OnboardSSI Issuer](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/OnboardSSI/-/blob/master/OnboardSSI-Issuer) | This constitutes a bridge between the OnboardSSI and Verity (Evernym Issuer/Verifier) which is provided as an example implementation for developers that want to leverage OnboardSSI with Evernym. This is an optional component required only in case of implementation with the Evernym SSI stack. In case of deployment of OnboardSSI with another SSI implementation there may be a need of a similar bridge to easily utilise the solution.
| [OnboardSSI Mobile SDK](https://gitlab.grnet.gr/essif-lab/infrastructure_3/quadible/OnboardSSI/-/tree/master/OnboardSSI-Mobile-SDK) | The Mobile SDK implementation fo the OnboardSSI that performs the strong identity verification based on an existing identity document, with High Level of Assurance based on the eIDAS regulation, and then forward the identity information to the OnboardSSI backend component to expose the information to the Issuer. |


# 8. Exploitation

Quadible is a startup that is currently in the process of trying to establish their market share in the industry of cybersecurity. The company has already signed as the first customer a Payment Provider and is currently in process of signing a Cooperative Bank. There are currently various opportunities in the pipeline of the company in sectors such as banking, insurance, healthcare, government and retail. Each of these industries provide high relevance to the continuous behavioural authentication product but also to the OnboardSSI, where a strong identity verification mechanism is required.

The OnboardSSI component provides the ability to onboard an existing identity document to the SSI with High Level of Assurance. Considering that the developed component through the eSSIF-Lab project provides the compatibility to be used even without the SSI, the existing customers and the ongoing opportunities have shown interest in incorporating the developed component into their product. The envisioned incorporatiion of the OnboardSSI could be done in two stages: at the first stage without the SSI where the OnboardSSI is integrated with the existing identity management infrastructure, and at the second stage by incorporating the whole SSI-based technologies and further the OnboardSSI. One of the targeted organisations is currently the Greek Government that already offers the ability to store the Driving License and the National Identity into a Gov.gr wallet.

Finally, the company is already exploring to further finance and develop the solution with relevant partners such as Ubitech and IDEMIA, by applying to different Horizon Europe calls in order to support, enhance and further develop the SSI concept.


# 9. Dissemination


### Speaking opportunity at the Fintech Fast Forward

<img src="dis/1_utG89iOLFZVOuyq1FvKPaw.jpeg" alt="drawing" width="500"/>


### Speaking opportuntiy at the GDSC HUA


Google Developer School Club constitutes an initiative developed by Google, that promotes and encourages students from various universities across the world to create their own groups, towards transferring the knowledge of novel technologies and concepts in the information technology area. Established industry experts are invited to speak and transfer the knowledge regarding novel approaches and mechanisms to the student, creating new collaborations, facilitating the students to enter the market and find the job opportunities that will excite them.
Quadible team was invited to speak to the Google Developer School Club at the Harokopio University of Athens, on the subject of the future technologies in authentication and identity management. Both the continuous behavioural authentication as well as the OnboardSSI were presented and discussed triggering the interest of the audience.

<img src="dis/gdschua.png" alt="drawing" height="500"/>


### Speaking opportunity at ICS2 Chapter
ICS2 Chapter constitutes a non profit organisation withe members in the cybersecurity area discussing about potential issues, exploitations and vulnerabilities. The chapter is focused on helping the members to improve their knowledge and skills by inviting experts in the area of cybersecurity to educate, train and inform the members on relevant novel technologies, ways of improving the defence of their organisation and educating them about new issues that may arise. The two co-founders of Quadible were invited to give a presentation regarding the common attacks used in the banking sector, how they take place, how the victims are tricked and then the type of technologies that Quadible is offering in terms of continuous behavioural authentication as well as the OnboardSSI component developed through the eSSIF-Lab project.

<img src="dis/isc2chapter1.png" alt="drawing" height="300"/> <img src="dis/isc2chapter2.png" alt="drawing" height="300"/> 



### Mobile World Congress
The Quadible team attended and exhibited at the 4YFN section of the Mobile World Congress in March 2022. During the 3 day conference the team spoke with different types of stakeholders such as potential customers, investors and potential partners. Several interesting conversations took 

<img src="dis/1646303552098.jpeg" alt="drawing" width="300"/> <img src="dis/1646303552123.jpeg" alt="drawing" width="300"/> 

<img src="dis/1646303550978.jpeg" alt="drawing" width="200"/> <img src="dis/1646303551436.jpeg" alt="drawing" width="200"/> <img src="dis/1646303545071.jpeg" alt="drawing" width="200"/>

# 10. Conclusion 

eSSIF-Lab has provided great assistance in the development of the OnboardSSI solution in terms of funding and feedback. The solution will continue to be maintained by Quadible as well as exploring opportunities to further develop the solution. Additional research and private funding opportunities are currently explored with partners from the eSSIF-Lab such as Ubitech but also external partners such as IDEMIA. The next step would be to develop the appropriate user interface and infrastructure to offer the solution as a Software-as-a-Service. This will allow the team to further extend the solution, develop additional components and perform the appropriate business development actions in order to facilitate the adoption of the solution by various commercial partners and customers.
