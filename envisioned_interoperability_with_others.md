# Envisioned Interoperability with Others

# Table of Contents
1. [Introduction](#1-introduction)
2. [eSSIF-Lab Components](#2-essif-lab-components)
3. [Contributions to SSI standardisation](#3-contributions-to-ssi-standardisation)

# 1. Introduction

For the wide adoption of the Self-Sovereign Identity (SSI) concept and in particular of eSSIF, user-friendliness is important while maintaining interoperability among the various components of the eSSIF ecosystem. Considering the re-usability of components in the process of developing additional functionalities, different type of technologies are used e.g. Aries, Indy etc. Thus, in order to provide functionalities and components that will be widely used by developers and solution providers, it is essential to ensure interoperability with relevant components.

**OnboardSSI** focuses on providing an easy-to-adopt and easy-to-use component that will allow users to convert their identity e.g. passport into Verifiable Credentials, with **[High Level of Assurance](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS+Levels+of+Assurance)** based on the eIDAS regulation. In terms of technology, the component ensures interoperability by:

- offering the SDK (Software Development Kit) to both Android and iOS mobile platforms,
- following the W3C standard to define the schema of the identity's attributes collected from the passport,
- providing easy integration with a few lines of code.


This document will provide details on actions, dissemination, communication activities performed through the **OnboardSSI** project within and outside the **eSSIF-Lab** project to ensure interoperability of the solution. The **OnboardSSI**, from the beginning of the project, has performed various activities to explore collaborations with other eSSIF-Lab sub-grantees to ensure interoperability within the **eSSIF-Lab** project, but also with external organisations, communities and groups to further enhance the interoperability of the solution and maximising the impact of the project.

# 2. eSSIF-Lab Components
The **OnboardSSI** team has already reached out to various relevant subgrantees of the eSSIF-Lab project in order to validate the interoperability of the technology.

| Project Name | Description |  Relevance |
| ------ | ------ | ------ |
| DOOR | Offers the ability to integrate hardware-based keys to strengthen the security of wallets | A wallet will incorporate the OnboardSSI SDK to seamlessly onboard users, while DOOR could ensure the secure access of the identity attributes of the wallet. |
| eIDAS Bridge | Enhances the legal certainty of any class of verifiable credential, by incorporating the issuer’s advanced or qualified electronic signature or seal. | OnboardSSI could enable the identity verification while eIDAS bridge will add the signature at the actual verifiable credential. |
| OpenUp | Open sourcing the Evernym products Verity, Mobile SDK, Connect.Me wallet. | OnboardSSI could leverage the open source products offered by Evernym to provide a wallet with seamless onboarding and secure continuous behavioural authentication. |
| Animo | Offers an easy to deploy SSI framework including Issuer, Verifier and Holder | OnboardSSI could integrate with Animo to offer strong identity verification for the Issuer. |
| IDEMIA | Offers a similar solution for onboarding users through identity verification | OnboardSSI could work together with IDEMIA in order to perform common SSI standarisation activities.|



# 3. Contributions to SSI standardisation

We will explore the potential contribution to SSI standardisation together with IDEMIA through:

* Verifiable Identity by EBSI by enabling the remote identity verification requirement for the subject’s and the document’s presence.
* Verifiable Presentation by EBSI by providing the ability through the verification method to inform the Verifier (e.g. based on BehavAuth) if the same person that was onboarded through the OnboardSSI is the same that provides the Verifiable Presentation to the Verifier. 
