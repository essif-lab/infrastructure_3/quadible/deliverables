# Evernym Functional Overview

# Table of Contents
1. [Introduction](#1-introduction)
2. [Functional Overview with Evernym](#2-functional-overview-with-evernym)

# 1. Introduction
The purpose of this section is to provide a high level overview of the phases taking place in SSI considering Evernym as the underlying SSI implementation. There are other existing SSI implementations to be considered for implementing an end-to-end demonstration of the fully integrated system that leverages **OnboardSSI**. For the purpose of this demonstration, Evernym SSI implementation has been selected. 
 
The functional elements of the system are split into five distinct phases in order to simplify the order and flow of interactions among the components of the system and to create an easier understanding of the overall system. Each of the phases is detailed below in a separate subsection. To simply understand the architecture of the system, the reader can consider that the different phases are executed in a sequential manner. In reality, the Communication Setup phase for the Issuer and the Verifier do not have to take place simultaneously; the Communication Setup phase between the Holder and the Verifier can take place also at a later stage but has to be executed before the Verification phase.

1. Communication Setup phase
2. Registration phase
3. OnboardSSI: Identity verification phase
4. Issuance phase
5. Verification phase

The novelty introduced by the **OnboardSSI** project is 3. Identity verification phase, that takes place at the wallet. After the successful identity verification process at the wallet, the passport data are sent to the **OnboardSSI** backend, placed at the Issuer, waiting for the Issuer to query the data and validate them internally through national databases, and if successful kickstart the Issuance phase.


# 2. Functional Overview with Evernym
This section provides the functional overview when the SSI implementation that is used is Evernym, while the phases defined below are mapped to the ones defined in Functional Overview of the Generic SSI Concept. As mentioned in the Generic SSI Concept, the novelty of the **OnboardSSI** lies in 3. Identity Verification phase. The rest of the phases are kept as is, apart from the defined schema that needs to be written to Sovrin.


## 2.1. Evernym: Establish a connection between Wallet and Issuer/Verifier
During this phase, the components such as the Wallet and the Issuer as well as the Wallet and the Verifier establish a pair connection in order to be able to communicate with each other. Evernym uses a Relationship protocol in order to establish a pair connection between the two components e.g. the Wallet and the Issuer. Initially, a relationship DID is created for the new relationship, while through an invite URL sent to the user, the Issuer waits for the Holder to initiate the Connection protocol and accept the connection. A graphical representation of the process and the protocol followed for establishing a connection between the Wallet and the Issuer/Verifier is provided [here](https://gitlab.com/evernym/verity/verity-sdk/-/blob/main/docs/verity-architecture/Generic_DID_Exchange__Connecting__Protocol_-_Verity_2.0.png)

## 2.2. Evernym: Write Schema
During this phase, the Issuer is writing the defined schema for the OnboardSSI to the Sovrin. The schema followed is the same as defined above in section 2.2.2 (Registration Phase) which is the [Verifiable ID Data Model](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/1.3.2.2.+Verifiable+Credentials+ESSIF+v2) schema defined by [EBSI-ESSIF](https://ec.europa.eu/cefdigital/wiki/display/EBSIDOC/1.3.2.2.+Verifiable+Credentials+ESSIF+v2). Following the Issuer Setup that has allowed the creation of an Issuer DID, a write schema request allows the initiation process for writing the predefined schema to Sovrin. Following the write schema request, there is a need to execute the write credentials definition method. At the point of writing this document, there is a need for an endorsement of the transaction by Evernym. Further information on how to write the schema through the Evernym Issuer is provided [here](https://gitlab.com/evernym/verity/verity-sdk/-/blob/main/docs/getting-started/java-verity-sdk.md#write-schema-to-ledger)

## 2.3. OnboardSSI: Identity verification phase
This phase is the same as in the Generic SSI Concept. The user performs the identity verification process through the **OnboardSSI** SDK at the wallet. The user kickstarts the process and the onboarding wizard is prompted requesting from the user to take an image of the passport, scan the passport through NFC, take a selfie with liveness detection and finally crosscheck the information of the document and the NFC chip. If the process is successful, the data retrieved from the passport are forwarded to the **OnboardSSI** backend placed at the Issuer. Once the data are uploaded to the **OnboardSSI** backend, the data are available for the Issuer to query, in order to validate the data with the national databases. If every check has been successfully completed, then the Issuance can start.  

## 2.4. Evernym: Issuance phase
During the Issuance phase, the corresponding Issue Credential protocol is triggered at the Evernym platform. Prerequisites are the initial setup of the Issuer during which the Issuer DID is created and then the Write Credential Definition protocols should have been executed and completed. For the Issue Credential protocol there are two steps required to be completed. The first one is comprised of a request called  "send credential offer" that is sent to the Holder, in order to offer the credentials. The second step requires the Holder to accept the credential offer, and once the Holder accepts the Issuer sends the Credential, which is the Verifiable Credential. Of course for this process to start, the Identity Verification process has to be completed in order to validate with the national databases, the data received from the **OnboardSSI**. More details about the process followed, are provided [here](https://gitlab.com/evernym/verity/verity-sdk/-/blob/main/docs/verity-architecture/Generic_Credential_Issuance_Protocol_-_Verity_2.0.png)

## 2.5. Evernym: Verification phase
During the Verification phase, where information corresponding to an existing Verifiable Credential is requested from a Verifier, then the Proof Presentation protocol is used. The protocol requires the Issuer DID as well as the DID for the relationship between the Wallet and the Verifier. A Presentation Proof request is sent through the Verifier to the Holder, including the aforementioned information as well as the attributes to be verified. In this way, the Verifier requests proof from the Holder. The response of this request includes the verification result informing the Verifier about the outcome of the request. More details about the process followed, are provided [here](https://gitlab.com/evernym/verity/verity-sdk/-/blob/main/docs/verity-architecture/Generic_Proof_Presentation_Protocol_-_Verity_2.0.png)
