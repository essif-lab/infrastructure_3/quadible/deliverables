# Functional Specification of OnboardSSI Component

# Table of Contents
1. [Introduction](#1-introduction)
2. [Architecture](#2-architecture)
	1. [System Components](#21-system-components)
	2. [Functional Overview](#22-functional-overview)
	3. [OnboardSSI](#23-onboardssi-sdk)
	4. [BehavAuth](#24-behavauth)
3. [eSSIF-Lab Functional Architecture Fitness](#3-essif-lab-functional-architecture-fitness)
4. [Use case: Know-Your-Customer (KYC) in Banking and Insurance](#4-use-case-know-your-customer-kyc-in-banking-and-insurance)

# 1. Introduction

As more and more technological advances are introduced in our lives, even more sophisticated cyber-attacks are observed. Identity and financial fraud reach daily headlines putting at risk citizens privacy as well as having large effect on citizens in terms of costs, increased stress and frustration. In parallel, cybersecurity has become a day-to-day hurdle for public sector and businesses that want to protect their organisation and customers as well as to comply with existing regulation. In some countries, governments, banks and insurance companies have introduced centralised approaches, for person identification and identity verification through electronic IDs. However, these approaches lack of transparency, are highly dependent on these organisations, do not provide ownership of identity to the person, lack interoperability and do not respect citizens privacy. 

Identity theft continues to pose challenges for consumers and organisations, as criminals develop new ways of committing fraud. In the meantime, public and private organisations undergo digital transformation that has been speed up with the effects of COVID-19, creating a shift towards offering the majority of services online. According to Experian, during COVID-19 lockdown there was a 33% increase in fraud rates. The Federal Trade Commission received 650,570 identity theft complaints in 2019, 46 percent increase from the previous year. 
Today, the most reliable mechanism for physical identification and authentication consists of requesting a photo-ID document and establishing a match with attributes of our physical appearance. The ID verifier rarely has the appropriate technology to verify that the physical-ID is legitimate and, only in very specific places, a biometric check can be successfully utilized. Virtual identification solutions have appeared, however their costs are huge (on average $2.12 per transaction), citizens do not own their identity data and the security measures taken for management and storage are questionable.

**OnboardSSI** will provide the first open source solution that allows strong identity verification with High Level of Assurance, allowing users to transform their physical identity into Verifiable Credentials. This document in focused on providing the overall architecture of the system including the various components that are involved in the process, the information flow among the various components as well as how the system fits in the overall eSSIF-Lab architecture.


# 2. Architecture

## 2.1. System components

**OnboardSSI** is designed in such a way to empower interoperability allowing the developer to leverage the technology through different SSI implementations. **OnboardSSI** reuses an existing SSI implementation including the Holder, Issuer and Verifier as described in the table below. The SSI implementation is not coupled with the **OnboardSSI** in a strict manner, for that reason the developers are capable of changing the underlaying implementation. 

The system includes two types of components in order to realise the **OnboardSSI**: a) the common Holder, Issuer and Verifier architecture based on an existing SSI implmentationand b) the components introduced in the **OnboardSSI** project. 

The table below provides the components that are reused, meaning an existing SSI implementation is leveraged.


| Component | Role | Functionality | 
| --------- | ---- | ------------- |
| Holder | Stores and presents the Verifiable Credentials | It is responsible for the communication with the Issuer to receive the attestated Verifiable Credentials but also to respond to any requests performed by the Verifier in order to generate the Disclosure and provide the Proof to the Verifier. |
| Issuer | Performs the attestation of attributes and generates the Verifiable Credentials | It is responsible for the communication with the Holder in order to receive the attributes to be attestated, to issue the Verifiable Credentials and send them back to the Holder. |
| Verifier | Conducts the verification of the Claims presented by the Holder | It is responsible to perform the communication with the Holder by requesting a Proof Presentation and receiving from the Holder a credential Proof regarding a particular Claim. |


The components that are introduced through the **OnboardSSI** project are shown in the table below:

| Component | Role | Functionality|
| ----- | ---- | ----- |
| **OnboardSSI** | Seamless onboarding of passports/ID cards | It is responsible for performing strong identity verification based on document and person verification at the device level, to retrieve with High Level of Assurance the attributes of the passport/identity and allow the Holder to forward them to the Issuer in order to generate Verifiable Credentials. The **OnboardSSI** is used in order to allow onboarding of the user to SSI leveraging an existing identity document.    |
| BehavAuth | Continuous behavioural authentication | It is responsible for providing high security in a user-friendly way at the wallet / Holder level. More than 14 unique behavioural traits continuously authenticate the user in order to ensure that only the correct user has access to the wallet. In this way, when a verification will take place the access to the wallet will be ensured through strong continuous behavioural authentication. | 

It should be mentioned that the selfie that has been taken during the onboarding process of the **OnboardSSI** will be used to create a profile at the BehavAuth behavioural authentication solution that protects the wallet. Then once the user accesses the wallet to perform a verification transaction then the unlocking of the wallet will take place only based on the confidence of the behavioural authentication. 

The components have been split into two categories in order to showcase to the reader that the introduced solutions are independent of any SSI implementation and can be implemented in any of the existing SSI implementations. In order to showcase the implementation of the proposed system in a complete SSI scenarion, the project will leverage the Holder, Issuer and Verifier provided by the OpenUp eSSIF-Lab subgrantee. In section 2.2 a generic functional overview of the system is provided with the appropriate abstraction (without focusing on a specific SSI implementation), while in section 2.3 the functional overview of the system is provided focused on Evernym implementation.


## 2.2. Functional Overview

The purpose of this section is to provide a high level overview of the phases taking place in SSI. There are existing protocols such as DIDComm and OIDC that implement these phases, however in order to provide a generic overview on the functionality of the system the description provided below is not specific to any of the protocols. 
The functional elements of the system are split into five distinct phases in order to simplify the order and flow of interactions among the components of the system and to create an easier understanding of the overall system. Each of the phases is detailed below in a separate subsection. To simply understand the architecture of the system, the reader can consider that the different phases are executed in a sequential manner. In reality, the Communication Setup phase for the Issuer and the Verifier do not have to take place simultaneously; the Communication Setup phase between the Holder and the Verifier can take place also at a later stage but has to be executed before the Verification phase.

1. Communication Setup phase
2. Registration phase
3. OnboardSSI: Identity verification phase
4. Issuance phase
5. Verification phase

The novelty introduced by the **OnboardSSI** project is 3. Identity verification phase, that takes place at the wallet. After the successful identity verification process at the wallet, the passport data are sent to the **OnboardSSI** backend, placed at the Issuer, waiting for the Issuer to query the data and validate them internally through national databases, and if successful kickstart the Issuance phase.

A clear description of the generic overview of the system information flow across the various components, without considering any specific SSI implementation, is provided in a separate guide in the [Generic Functional Overview](generic_functional_overview.md). In order to demonstrate to the reader an actual implmentation and integration of the OnboardSSI with existing implementation, e.g. Evernym, the reader is directed to the [Evernym Functional Overview](evernym_functional_overview.md).


## 2.3. OnboardSSI


### 2.3.1. Role of OnboardSSI
**OnboardSSI** focuses on seamless allowing users and citizens to remotely onboard their existing identity through an identity document, with High Level of Assurance based on the eIDAS regulation. The user, through a simple wizard is able to onboard their identity and sent it to a backend component of the **OnboardSSI**, that is placed at the Issuer's side. Then the Issuer is able to query the elements of the identity of the person that was verified at the device level with High Level of Assurance, crosscheck the data with national databases and upon successful validation, issue the Verifiable Credentials.

### 2.3.2. How it works
**OnboardSSI** is a Software Development Kit (SDK) that is responsible for strongly verifying the identity document of a person at the device level. It retrieves the identity attributes to be forwarded to the Issuer for further validation and generation of Verifiable Credentials. The steps performed to verify the document validity and the identity of the person lead to the High Level of Assurance based on the eIDAS regulation. It constitutes a security check at the device level, before forwarding the attributes of the identity document e.g. passport, ID-card, to the Issuer, towards further validating the attributes with national databases and at the end issuing Verifiable Credentials.

![OnboardSSI information flow](imgs/onboardssi.png)

The **OnboardSSI** SDK is available for mobile platforms, allowing developers to integrate the solution into their mobile apps in order to perform identity verification at the device level with a High Level of Assurance based on the eIDAS regulation. To verify an identity document of a person, an onboarding wizard is prompted to the user that includes the following steps:

1. **Selection of the identity document**. Depending on the type of document, different verification is triggered. The initial version of the system will include the verification of a European passport with an NFC chip integrated. The NFC chip allows the system to achieve the High Level of Assurance (eIDAS), if the NFC is not available then only a Moderate Level of Assurance can be achieved and in most cases, there is a need for a video-based verification.
2. **Photo of the document**. A camera screen is prompted to the user in order to take a clear image of the identity document information. Guidance is provided to the user in order to capture a clear image of the document including the appropriate information required for the verification process. AI-based Optical Character Recognition technology is able then to retrieve the identity information from the document.
3. **NFC scan of the document**. The user is requested to place the smartphone on top of the NFC-enabled identity document. Through the NFC technology, the system is able to retrieve the information stored on the chip. This allows the comparison of the identity data of the NFC chip and the one displayed on the document itself, preventing any fraud cases or tampering of the identity data.
4. **Selfie**. The user is prompted with a screen to take a selfie through the frontal on-device camera of the smartphone. Once the screen is visible a face detection algorithm initiates the process of finding the face of a person. Once a face is detected, the liveness detection process is executed to understand if the face visible on the screen is a true person and no presentation attack is taking place. Presentation attack refers to the process of a malicious user showing in the camera a fake image or a video. If the liveness detection succeeds the user is allowed to take a selfie. Through the face matching algorithm, the selfie image is compared with the image retrieved from the passport through the NFC as well as the image is shown on the document.
5. **Final validation**. During the final stage of the verification process, the information is cross-validated both in terms of the identity information that are presented on the document but also in terms of the image of the person that performs the identity verification. Upon successful validation, the information is forwarded to the Issuer for further verification of the document but also for the issuance of the Verifiable Credentials.

## 2.4. BehavAuth

### 2.4.1. Role of BehavAuth
BehavAuth is an AI-platform that continuously authenticates users, without the need for any user input, by learning their behavioural patterns. In the SSI concept, once a user has performed strong identity verification through the **OnboardSSI** and received the Verifiable Credentials, the VCs are stored at the user's wallet.  At the moment a Verifier requests the proof from the wallet, it is essential that only the correct user has access to the wallet, to provide the proof to the Verifier. This is ensured by the Strong Authentication mechanism provided by BehavAuth. Through continuous behavioural authentication, BehavAuth will continuously ensure that only the correct user unlocks the wallet and provide the proof to the Verifier. It should be noted that the initial profile creation for the BehavAuth is performed by leveraging the face image of the user retrieved by the Strong Identity Verification performed by **OnboardSSI**. While a user is accessing the wallet, continuous behavioural authentication ensures through the whole user session that only the correct user has access. If BehavAuth detects that the wallet is accessed by another user the wallet will be locked (a fallback mechanism could be also leveraged).


### 2.4.2. How it works

BehavAuth focuses on authenticating users for third-party platforms. For the design of the authentication system, state-of-the-art architecture design patterns were considered from IEEE Centre for Secure Design. The authentication process is based on understanding and learning the behavioural patterns of the users by leveraging data extracted from smartphones, such as sensor and device usage data. BehavAuth constitutes the platform, where the processing and the authentication procedure takes place and the mobile wallet app/library, where the data are being collected in order to send them to the back-end platform. 

![BehavAuth information flow](imgs/behavauth-architecture.png)

The first time the app is launched, the users take a photo of their face. To minimise the steps required, the image taken from the **OnboardSSI** will be leveraged directly after the successful identity verification. Through that image, a profile will be created at the BehavAuth platform. The data are collected from the wallet app and are forwarded to the BehavAuth platform. The BehavAuth platform will, as a first step, create a record of the pseudo-ID and initiate the learning process for the authentication models. Extremely accurate face recognition is used to authenticate the user until the behavioural patterns are learned. Once the behavioural models have learned the patterns of the user, they are incorporated into the authentication process. The system performs authentication followed by learning, in order to adapt to the contextual changes of the person. The information flow among the different system components is shown in the picture above. The overall system consists of the Issuer/Verifier and the BehavAuth platforms, and a mobile wallet app (which includes the BehavAuth and **OnboardSSI** components). The  mobile wallet app will integrate the BehavAuth library. The latter will constitute the component that collects the behavioural data from the smartphone, encrypts them (end-to-end encryption) and then through the secure communication channel it has established, will propagate the behavioural data to the BehavAuth platform, in order to perform authentication and inform the wallet mobile app that this is the actual user and not an imposter, by generating a risk score. If the user is corrent then he/she are able to access the wallet and perform any issuance or verification of credentials. If the user is an imposter, the wallet app will be locked to prevent any unauthorised access. 


#3. eSSIF-Lab Functional Architecture Fitness


**OnboardSSI** fits in the Functional Architecture of the eSSIF-Lab at the wallet level. SSI provides the ability to the users to get full control over their identity and to receive a strong guarantee/attestation from the Issuer regarding the validity and verification of the attributes of the identity. Adopting such novel technologies requires a well-designed bridge with existing methodologies and approaches, creating in a sense backwards compatibility. Currently, identity documents such as passports and ID cards are the dominant verification means for people in society. For this reason, there is an imperative need to allow people to migrate their physical identity documents into the digital world and in particular into the SSI context.

The sole purpose of the project is to allow citizens to transform their existing identities into the world of SSI, in an easy and seamless manner. As a starting point, **OnboardSSI** will provide a secure and user-friendly mobile wallet that allows:

1. seamless onboarding for users, 
2. easy creation of verifiable credentials from existing identity documents such as passports, 
3. secure storage of verifiable credentials through strong encryption, 
4. appropriate abstraction for user-friendly management of the verifiable credentials even by non-technical people and 
5. highly secure access control for the wallet through AI-powered continuous behavioural authentication.

**OnboardSSI** fits in the eSSIF-Lab Functional Architecture through the Wallet component. It strongly verifies the identity of the user with a High Level of Assurance, and through the Wallet and the Holder, it creates SSI-compatible attributes of the identity based on the onboarded document, which are then forwarded to the Issuer for the Credential Issuance. Once the Verifiable Credentials are generated, they are sent and stored in the Wallet. During the whole access to the Wallet, the keys and the information of the wallet are protected by continuous behavioural authentication.

![Fit in the eSSIF-Lab architecture](imgs/essif-lab-architecture-fit.png)


# 4. Use case: Know-Your-Customer (KYC) in Banking and Insurance

In the pre-COVID era, the majority of the banks and the insurance companies required from their customers to physically attend a branch in order to verify their identity and create a bank account or an insurance policy. In the post-COVID era, banks have been forced to offer the ability to their customers, to remotely create a bank account, requiring a Moderate or High Level of Assurance based on the eIDAS regulation. Several banks are currently moving towards incorporating the SSI concept. Thus it is essential for the banks customers to easily onboard their existing identity to the SSI and generate Verified Credentials. The majority of the insurance companies do not offer the remote insurance policy creation due to the lack of strong identity verification. 

Towards this end, **OnboardSSI** will offer to the banks and insurance companies 

1. an SDK that will integrate with the mobile app (e.g. mobile banking or mobile insurance app). The SDK will allow their customers to easily onboard to SSI their existing identity through a passport, and generate Verifiable Credentials,
2. through the collaboration with other eSSIF-Lab subgrantees e.g. Evernym, an Issuer will be provided to the banks and insurance companies that will manage the issuance of the Verifiable Credentials,
3. similarly a Verifier will be provided to the banks that allows the verification of the identity Proof.







