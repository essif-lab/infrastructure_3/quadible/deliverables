# Generic Functional Overview

# Table of Contents
1. [Introduction](#1-introduction)
2. [Functional Overview in Generic SSI Concept](#2-functional-overview-in-generic-ssi-concept)

# 1. Introduction
The purpose of this section is to provide a high level overview of the phases taking place in SSI. There are existing protocols such as DIDComm and OIDC that implement these phases, however in order to provide a generic overview on the functionality of the system the description provided below is not specific to any of the protocols. 

# 2. Functional Overview in Generic SSI Concept
The functional elements of the system are split into five distinct phases in order to simplify the order and flow of interactions among the components of the system and to create an easier understanding of the overall system. Each of the phases is detailed below in a separate subsection. To simply understand the architecture of the system, the reader can consider that the different phases are executed in a sequential manner. In reality, the Communication Setup phase for the Issuer and the Verifier do not have to take place simultaneously; the Communication Setup phase between the Holder and the Verifier can take place also at a later stage but has to be executed before the Verification phase.

1. Communication Setup phase
2. Registration phase
3. OnboardSSI: Identity verification phase
4. Issuance phase
5. Verification phase

The novelty introduced by the **OnboardSSI** project is 3. Identity verification phase, that takes place at the wallet. After the successful identity verification process at the wallet, the passport data are sent to the **OnboardSSI** backend, placed at the Issuer, waiting for the Issuer to query the data and validate them internally through national databases, and if successful kickstart the Issuance phase.

## 2.1. Communication Setup phase
This phase is responsible for the establishment of the initial communication between two components e.g. the Holder and the Issuer or the Holder and the Verifier. Assuming that this phase is executed to establish the communication/relationship between the Holder and the Issuer. It should be noted that the same process will be followed for the establishment of the communication/relationship between the Holder and the Verifier. During this phase, each of the two components generates a pair of Keys (public and private), e.g. Kk for the Issuer and Aa for the Holder that will be used to verify the communication between the two components. The private keys (i.e. a and k) are stored locally at each component while the public keys (i.e. A and K) are exchanged and stored locally. Only the Issuer stores its public key on the Ledger. Now the two components are able to communicate having an 'official' relationship. For establishing the communication there are existing protocols that could be implemented and still provide compatibility at the system such as PeerDID, KERI or OIDC-SIOP.

![Communication Phase information flow](imgs/communication-phase.png)

## 2.2. Registration phase
The Registration phase is a prerequisite for the Issuance and Verification phase. This is because the Registration phase is responsible for the creation of the Schema and for writing it on the Ledger. The Schema has been predefined in the actual system, during the system architecture preparation, before the beginning of the execution of the system. The Schema is responsible for the definition of the structure of the Verifiable Credentials that will be stored in the wallet of the user. In order to ensure interoperability of the system with existing components, the [Verifiable ID Data Model](https://ec.europa.eu/digital-building-blocks/wikis/display/EBSIDOC/1.3.2.2.+Verifiable+Credentials+ESSIF+v2) schema is defined by [EBSI-ESSIF](https://ec.europa.eu/cefdigital/wiki/display/EBSIDOC/1.3.2.2.+Verifiable+Credentials+ESSIF+v2) has been used.

![Registration Phase](imgs/registration-phase.png)

An example that leverages the defined schema is shown below:

```
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://essif.europa.eu/schemas/vc/2020/v1",
        "https://essif.europa.eu/schemas/eidas/2020/v1"
    ],
    "id": "did:ebsi:00000001/credential/1872",
    "type": [
        "VerifiableCredential",
        "VerifiableID"
    ],
    "issuer": "did:ebsi:00000001",
    "issuanceDate": "2019-06-22T14:11:44Z",
    "credentialSubject": {
        "id": "did:ebsi:00000002",
        "currentFamilyName": "Skywalker",
        "currentFirstName": "Luke",
        "dateOfBirth": "1999-03-22T00:00:00Z"
    },
    "credentialStatus": {
        "id": "https: //essif.europa.eu/status/42",
        "type": "CredentialsStatusList2020"
    },
    "credentialSchema": {
        "id": "https://essif.europa.eu/tsr-vid/verifiableid.json",
        "type": "JsonSchemaValidator2018"
    },
    "evidence": [{
        "id": "https://essif.europa.eu/evidence/f2aeec97-fc0d-42bf-8ca7-0548192d4231",
        "type": ["DocumentVerification"],
        "verifier": "https:// essif.europa.eu /issuers/48",
        "evidenceDocument": "Passport",
        "subjectPresence": "Physical",
        "documentPresence": "Physical"
    },{
        "id": "https://essif.europa.eu/evidence/f2aeec97-fc0d-42bf-8ca7-0548192dxyzab",
        "type": ["SupportingActivity"],
        "verifier": "https://example.edu/issuers/48",
        "evidenceDocument": "Fluid Dynamics Focus",
        "subjectPresence": "Digital",
        "documentPresence": "Digital"
    }],
    "proof": {
        "type": "EidasSeal2019",
        "created": "2019-06-22T14:11:44Z",
        "proofPurpose": "assertionMethod",
        "verificationMethod": {
            "type": "EidasCertificate2019",
            "CertSerial": "1088321447"
        },
        "proofValue": "BD21J4fdlnBvBA+y6D...fnC8Y="
    }
}
```

## 2.3. OnboardSSI: Identity verification phase
The Identity Verification phase is the process of strongly verifying the identity of the wallet owner when a new identity document (credential) is being onboarded to be sent to the Issuer and in the end to be converted into a Verifiable Credential. The end-user kicks off the onboarding process of an identity document e.g. a passport, through the mobile wallet stored on a smartphone. The onboarding process includes a few steps to verify the validity of the identity document but also of the person that is performing the onboarding of the identity through liveness detection and face matching. A more detailed description of the actual process performed for the identity verification is provided in the following Section OnboardSSI SDK. If the verification process is successful, the information from the identity document is ready to be sent to the Issuer.  

## 2.4. Issuance phase
This phase is responsible for issuing the Verifiable Credentials by the Issuer and sending them to the Holder. A prerequisite of this phase is initially the establishment of the relationship between the Holder and the Issuer through the Communication Setup and also the Registration phase that will register the defined schema to the Ledger. During this phase, the Holder sends to the Issuer the attributes of the verified identity. The Issuer internally checks the validity of the received identity attributes with respect to the national databases. Once the validity of the identity is confirmed, the Issuer goes forward with the issuance of the Verifiable Credentials with respect to the predefined Schema. Following the issuance of the Verifiable Credentials, they are sent to the Holder in order to store them in the user's wallet. 

![Issuance Phase flow](imgs/issuance-phase.png)

## 2.5. Verification phase
The final phase of the flow is the Verification Phase, which is responsible for the verification process of the Verifiable Credentials that may be requested by a particular Verifier. This phase assumes that all the aforementioned phases have been executed including the Communication Setup between the Holder and the Verifier. At any point in time, a Verifier can request a Proof of Presentation from the Holder regarding certain aspects of the user's identity. Following the request from the Verifier, the Holder generates a Credential Disclosure and forwards the Credential Proof to the Verifier. The Verifier retrieves the public keys from the Ledger (Data Registry) and verifies the received Proof.

![Verification Phase flow](imgs/verification-phase.png)