# Interface Specification of OnboardSSI Component

# Table of Contents
1. [Introduction](#1-introduction)
2. [API Specs](#2-api-specs)
	1. [Communication Setup Phase](#21-communication-setup-phase)
	2. [Registration Phase](#22-registration-phase)
	3. [Identity Verification Phase](#23-identity-verification-phase)
	4. [Issuance Phase](#24-issuance-phase)
	5. [Verification Phase](#25-verification-phase)
3. [Sequence Diagrams](#3-sequence-diagrams)
4. [Interface Specifications](#4-interface-specifications)
	1. [OnboardSSI SDK interface](#41-onboardssi-sdk-interface)
	2. [OnboardSSI Backend](#42-onboardssi-backend)
	3. [BehavAuth SDK interface](#43-behavauth-sdk-interface)


# 1. Introduction
This document describes the way on how the various components of the SSI context interface with each other in order to provide the whole functional process defined for the OnboardSSI system to operate. Core elements of the SSI ecosystem are involved in the interface specification to provide a 360 view on how the system operates, including all the necessary phases to create Verifiable Credentials but also verify them. 

In order to showcase a complete system that integrates the **OnboardSSI** solution, another subgrantee project will be used that offers the complete components required for the implementation of the SSI infrastructure i.e. Issuer, Verifier, SSI Mobile SDK for the implementation of a wallet. In particular, the SSI components that Evernym provides will be leveraged, in order not to re-invent the wheel, but to build on existing and one of the most widly spread SSI solutions. The architecture of the system has been defined in such way to allow third-party developers to easily substitute the SSI components that Evernym offers with other SSI solutions such as Trinsic, Sphereon, Animo etc. The focus has been on creating an interoperable solution that is independent of the underlining SSI infrastructure implementation. 

OnboardSSI allows the integration with any SSI implementation infrastructure without limiting the solution particular vendors and without locking the developer into specific technologies. The interoperability has been taken into account already from the architecture of the system but also will be also showcased through the interoperability testing activities performed throughout the project. To ensure the interoperability of the system, the solution is comprised by two key components: (a) the **OnboardSSI** Mobile SDK that is responsible for the strong identity verification with High Level of Assurance based on the eIDAS regulation and (b) the **OnboardSSI** backend that will be placed at the Issuer's side in order to retrieve the attributes of the identity document (e.g. passport, ID-card) and to forward that information to the Issuer for further cross-check of the data and to generate the Verifiable Credentials. The communication between the **OnboardSSI** mobile SDK and the backend component will be conducted through REST APIs. The details of this process is presented in the Identity Verification Phase below.

# 2. API Specs

To assist the reader in understanding the architecture and interfaces among the various components involved in the **OnboardSSI** solution, a detailed description of the interfaces is provided below, which has been split into separate phases. The phases involved have been already defined in the architecture of the system and should be followed in a chronological manner. Meaning that the sequence in which the phases are shown should be kept as-is and the corresponding sequence should be also maintained in the implementation process. In cases where, the description of the API functionality explicitly mentions the ability to modify the sequence of the calls, there the developer is able to do so. Furthermore, the phases are inline with the SSI concept defined by the Sovrin foundation and ESSIF. 

Each phase includes a separate table that indicates the involved components i.e. which components interface among each other during the particular phase. An interface between two major components e.g. wallet and Issuer could be split into two separate communications Wallet-Mobile SSI SDK and then the Mobile SSI SDK-Issuer in order to define the interfaces and interactions of subcomponents but also allow the reader to get deeper knowledge of the involved components enabling the potential substitution of components removing any vendor dependency. The tables include a specific column about the exposed interface that links to the technical documentation of the interface exposed. The column description provides an overview of the actions taking place through these interfaces, during that phase. 

At the end of the document the various sequence diagrams are provided for each of the phases involved in the process for the setup of each of the components Holder/Wallet, Issuer, Verifier and Ledger as well as the issuance and verification of the Verifiable Credentials.

## 2.1. Communication Setup Phase

| Involved Components | Interface | Description |
| ---------- | ---------- | ---------- |
| Wallet - Evernym SSI Mobile SDK | [Establish a connection](https://github.com/evernym/mobile-sdk/blob/main/docs/5.Connections.md) | The initial step constitutes the process for kickstarting the strong identity verification and the issuance of Verifiable Credentials based on High Level of Assurance document onboarding, is to setup the connection acceptance from the wallet to the SSI Mobile SDK and further the Inviter which is either the Issuer or the Verifier. The Inviter sends a connection invitation to the SSI Mobile SDK which in turn, through the explicit consent of the user, checks if the connection exists (*connectionInviteDetails*) and then accepts the connection (*vcxConnectionConnect*). A connection object is being created (*connectionSerialize*) that allows to check is the connection has been established (*vcxConnectionUpdateState*), to send and receive messages, even to delete the connection(*deleteConnection*). The connection object can be serialised and saved at the application storage for future use in order to communicate with the Issuer or the Verifier. During the connection process, the entities exchange their Public keys, which are stored internally at each of the SSI components and could be written to the Ledger (Sovrin), creating a binary relationship between them. Through the SSI Mobile SDK the appropriate abstraction is offered for the communication between the Holder/wallet and the Verifier/Issuer.|
| Issuer (Verity) - Evernym SSI Mobile SDK | [Verity Rest API](https://app.swaggerhub.com/apis/evernym/verity-rest-api/1.0#/) | A similar approach as described in the above section  (Wallet - EvernymSSI Mobile SDK), however the detail is placed on the Issuer's side. The initial action required by the Issuer is to perform issuer setup (*/api/{domainDID}/issuer-setup/0.6/{threadId}*) in order to generate the Issuer DID and the pair of public and private keys that will be used to issue credentials. The DID as well as the public key are written to the Ledger (Sovrin) in order to guarantee the immutability of these elements and to expose them to the world. In this way, the Verifier has the ability to cross-check that the DID and the Public key belong to the particular Issuer. Having generated the DID and the public key, now the Issuer is ready to create a relationship (*/api/{domainDID}/relationship/1.0/{threadId}*) with the wallet in order to start to exchange messages. During the relationship setup, the Issuer sends an invitation to the wallet to establish a communication/relationship. The user explicitly accepts the invitation, where the two components exchange their DIDs and the public keys. The issuer stores the DID and public key of the Holder and is now able to exchange messages with the Holder. |
| Verifier (Verity) - Evernym SSI Mobile SDK | [Verity Rest API](https://app.swaggerhub.com/apis/evernym/verity-rest-api/1.0#/) | For the Verifier, almost the same approach is followed to the Issuer apart from the Issuer setup process. The Verifier creates initially the DID and the Public key that will be used in the relationship between the Verifier and the Holder towards securely exchanging messages between them. Both the DID and the Public Key of the Holder are stored locally at the Verifier and are written to the Ledger (Sovrin). The Verifier initiates, through an invitation, the relationship generation (*/api/{domainDID}/relationship/1.0/{threadId}*) by sending a request message to the Holder. The holder, through explicit action of the user, accepts the relationship creation with the Verifier and stores the connection object in a serialised manner at the application storage. Through this connection object the Verifier is able to perform presentation proof requests to the Holder in order to verify particular information related to the Verifiable Credentials stored at the wallet. | 


## 2.2. Registration Phase

| Involved Components | Interface | Description |
| ---------- | ---------- | ---------- |
| Issuer (Verity) - Ledger (Sovrin) | [Verity Rest API](https://app.swaggerhub.com/apis/evernym/verity-rest-api/1.0#/) | This phase is responsible for writing the schema of the solution to the Ledger, in this case Sovrin. A predefined schema has already been shaped by the OnboardSSI team base on the W3C standard in order to maintain interoperability with other SSI systems. The schema includes the information retrieved by the identity documents such as a passport or ID-card, in a structured format. Following the establishment of a connection of the Issuer with the Ledger (Sovrin), the system leveraging the Verity Rest API will initially create and write the schema on the Ledger (*/api/{domainDID}/write-schema/0.6/{threadId}*). The next step would be to create and write the credential definition (*/api/{domainDID}/write-cred-def/0.6/{threadId}*) to the Ledger (Sovrin) with the corresponding http POST call. In this way, the appropriate information has been written to the Ledger, and the system is ready to establish connections with Holders in order to issue credentials. |

## 2.3. Identity Verification Phase

| Involved Components | Interface | Description |
| ---------- | ---------- | ---------- |
| Wallet - Evernym SSI Mobile SDK - OnBoardSSI | OnboardSSI API | This phase takes place mainly on the device level and in particular at the wallet. The user initiates the process of an identity document verification through the mobile wallet app. During this process a wizard is prompted to the user to (1) select the type of document that will be verified, (2) scan the identity view of the document through state-of-the-art Optical Character Recognition (OCR), (3) scan the identity document through NFC to retrieve the information, (4) face detection, liveness and face matching, (5) cross-check of the information retrieved and validation of the document information. Having performed the document onboarding with High Level of Assurance, the information of the document are sent through HTTPS to the backend component of the OnboardSSI, placed at the Issuer (*/api/identity-document*/). The backend component of the OnboardSSI will temporary store and expose the identity data to the Issuer. The Issuer, through the OnboardSSI API will have the ability to retrieve either the identity document unique number or all the identity information retrieved from the onboarding process. In case all the identity information is requested from the OnboardSSI API, the Issuer (is a government entity) will have the ability to cross-correlate the data of the document with the data stored in the national databases. |

## 2.4. Issuance Phase

| Involved Components | Interface | Description |
| ---------- | ---------- | ---------- |
| Wallet - Evernym SSI Mobile SDK | [Request credential](https://github.com/evernym/mobile-sdk/blob/main/docs/4.MessagesFlow.md) and  [Accept credential](https://github.com/evernym/mobile-sdk/blob/main/docs/6.Credentials.md) | Following the strong identity verification process through the onboarding of the identity document with High Level of Assurance, the Holder is downloads all requests from the Issuer for Verifiable Credentials (*vcxGetMessages*). The attributes that have been extracted from the Identity Verification Phase has been sent as a message to the backend that is placed at the Issuer. The Holder sends a credential request to the Issuer in order to generate the Verifiable Credentials. Then the Holder waits for the Issuer to send a Credential Offer. Once it is received the Holder parses the information of the message (*credentialDeserialize*) and presents to the user that credential offer request has been received. If the user accepts the credential offer, then a credential request (*credentialSendRequest*) is sent in order to receive the Verifiable Credential. Once the Verifiable Credential has been received (*credentialUpdateState*), a presentation is generated and stored at the wallet (*credentialSerialize*). If the credential offer is rejected then a credential reject message is sent to the Issuer. |
| Evernym SSI Mobile SDK - Issuer (Verity) | [Verity Rest API](https://app.swaggerhub.com/apis/evernym/verity-rest-api/1.0#/) | The initial step that triggers the issuance phase is the message that the Holder sends to the Issuer including the attributes received by the strong identity verification with High Level of Assurance. The Issuer internally cross-check the information received from the passport through national databases and then kickstarts the process for issuing Verifiable Credentials (*/api/{domainDID}/issue-credential/1.0/{threadId}*). The Verifiable Credentials that will be issued, will take into account that a strong identity verification process has taken place at the Holder's side. Potential collaboration with Sphereon (othe subgrantee) will enable the integration with eIDAS by attaching a seal after the Verifiable Credentials have been issued. In this way, an additional layer of assurance will be added at the Verifiable Credentials level. | 


## 2.5. Verification Phase

| Involved Components | Interface | Description |
| ---------- | ---------- | ---------- |
| Verifier (Verity) - Evernym SSI Mobile SDK | [Verity Rest API](https://app.swaggerhub.com/apis/evernym/verity-rest-api/1.0#/) | Having established a connection with the Holder at the Communication Setup phase, the Verifier  is able to perform a verification of particular credentials of interest. To achieve this, the Verifier performs a proof request call to the Holder. The proof request includes various attributes such as the name of the information to be verified (e.g. name, surname, age) and that the attestation has been done by the Issuer. The request is send to Holder and the Verifier waits to receive the digital proof. Once the digital proof is received the Verifier performs the validation process leveraging the public keys of the Issuer available on the Ledger (Sovrin). Once the verification process has been completed, the result of the verification process is available through the proof presentation API. Through the REST API, it is able to query for the result of the verification, performed by the Verifier, based on the proof and the attributes requested. | 
| Wallet - Evernym SSI Mobile SDK | [Respond to a proof request](https://github.com/evernym/mobile-sdk/blob/main/docs/7.Proofs.md) | Once the Holder receives a request for proof presentation , the proof generation is kick-started (*/api/{domainDID}/proof-presentation/1.0/{threadId}*). The Holder downloads and parses the proof request that was sent by the Verifier. Then, a query is performed to the secure storage of the wallet in order to retrieve the appropriate Verifiable Credential that is relevant to the proof request sent by the Verifier. If the Verifiable Credential was found at the secure storage, the corresponding proof presentation is generated and sent to the Verifier. If the Verifiable Credential was not found, a proof rejection is sent to the Verifier. |


# 3. Sequence Diagrams

This section depicts the actions performed across the various components involved in the onboarding of an identity document to the SSI ecosystem with High Level of Assurance based on the eIDAS regulation. Considering the classification performed across the provided documents, the same phase-based classification will be kept across the sequence diagrams. As mentioned before, each of the phases are presented in a chronological timeframe thus the developer so keep the same order of actions and interface engagement as shown in the diagrams.

![SSI Sequence](imgs/ssi-sequence.png)


# 4. Interface Specifications
This section provides a description of the interfaces for the OnboardSSI solution as well as the BehavAuth solution. Initially the interfaces for the mobile OnboardSSI SDK are provided followed by a Swagger API Description for the OnboardSSI backend. Finally the interface that is exposed by the BehavAuth SDK in described in the last subsection. 

## 4.1. OnboardSSI SDK interface
This section provides the interface exposed by the mobile SDK to the wallet app, in order to kickstart the strong identity verification process and to get the result of the procedure.

The interface for the `OnboardSSI` class is provided below.

| Methods | Parameters | Description |
|-------|-----------|-----------|
|startPassportAdditionFlow          | No parameters |Opens the screen which will guide the user to add a document.|

| Fields | Values | Description |
| ------ | ------- | ----------- |
|documentAdditionResult|A [`Flow`](https://kotlinlang.org/docs/flow.html) that emits `DocumentAdditionResult` entries|This fields is needed in order to listen about the state of the document addition process. The emission happens once and only for the suscribed collectors. New collectors won't get previous results.

| Enums | Values |
|-----|-------|
|DocumentAdditionResult|`SUCCEED`: Indicates that a document was added successfully.<br/>`FAILED`: Indicates that a failure occured and a document could not be added.<br/>`CANCELED`: Indicates that document addition process was canceled by the user.|  

```kotlin
val documentAdditionResult: Flow<DocumentAdditionResult>

fun startPassportAdditionFlow()
```

## 4.2. OnboardSSI Backend
The OnboardSSI Backend is responsible for receiving the information from the successful identity verification of the OnboardSSI SDK at the wallet. The OnboardSSI Backend should be deployed at the Issuer. This will allow the Issuer to retrieve the information verified at the wallet of the user, in order to crosscheck the information with the national databases and finally begin the process of issuing verifiable credentials from the existing identity document such as the passport. 

The `yaml` file for the Swagger API description of the OnboardSSI backend is provided below. To get a visual representation of the Swagger API description, the reader is encouraged to input the following description into the [Swagger Editor](https://editor.swagger.io/)

```YAML
swagger: '2.0'
securityDefinitions:
  masterKey:
    type: apiKey
    in: header
    name: x-master-key
  token:
    type: apiKey
    in: header
    name: authorization
  apiKey:
    type: apiKey
    in: header
    name: x-api-key
info:
  version: 1.0.0
  title: OnboardSSI API
  description: 'Documentation of the API used for document verification'
  termsOfService: ''
consumes:
  - application/json
  - multipart/form-data
produces:
  - application/json
tags:
  - name: DocumentManagement
  - name: UserAuthentication
paths:
  
  /api/users/authenticate:
    post:
      operationId: UserEndpoints.authenticate
      summary: "Used by client to acquire authentication token"
      tags:
        - UserAuthentication
      parameters:
        - in: body
          name: body
          required: false
          schema:
            $ref: '#/definitions/VerifiedLoginRequestModel'
      responses:
        '200':
          description: Success
          schema:
            $ref: '#/definitions/VerifiedLoginModel'
        '403':
          description: Forbidden
      security:
        - apiKey: []
        
  /api/documents/uploadDocument:
    post:
      operationId: DocumentManagement.pushDocumentInfo
      summary: "Used by client to upload verified document details"
      tags:
        - DocumentManagement
      parameters:
        - in: body
          name: body
          required: false
          schema:
            $ref: '#/definitions/DocumentData'
      responses:
        '200':
          description: Success
      security:
        - token: []
        
  /api/documents/validateDocument/{id}:
    get:
      operationId: DocumentManagement.validateDocument
      summary: "Used by issuer to verify document details"
      tags:
        - DocumentManagement
      parameters:
        - in: path
          type: string
          name: id
          required: true
      responses:
        '200':
          description: Success
          schema:
            $ref: '#/definitions/DocumentData'
      security:
        - masterKey: []
  
definitions:
  DocumentData:
    properties:
      documentId:
        type: string
      documentType:
        type: string
      documentData:
        type: object
  
  VerifiedLoginModel:
    properties:
      userId:
        type: string
      authorizationToken:
        type: string
    type: object
    
  VerifiedLoginRequestModel:
    properties:
      userId:
        type: string
      verificationSecret:
        type: string
    type: object
```


## 4.3. BehavAuth SDK interface

The BehavAuth SDK is responsible for implementing the continuous behavioural authenticatio at the wallet level. The SDK collects behavioural data and offloads them to the BehavAuth cloud platform in order to perform the behavioural analysis and finally generate a risk score at any point of time regarding the authenticity of the user.

The interface for the BehavAuth class is provided below.

| Method name | Parameters | Description|
|-------------|------------|-------------| 
| getInstance | No parameters | Returns the singleton instance of the BehavAuth class|
| install | `android.app.Application` app,	`String` apiKey, `String` apiSecret, `int` mode | Setup the BehavAuth library, to start the data collection and processing. For ‘mode’ see constants below. |
| start() | `BehavAuthPrompt.RegistrationCallback` callback | Start the BehavAuth continuous authentication. If it is the first time it is launched, then it will perform a registration and initialisation process. |
| stop() | No parameters | Stop the continuous authentication when the app is in the background or when it is closed/killed. |
| setCallback() |`BehavAuthPrompt.AuthenticationCallback` callback | Set the callback to inform the application when the user is legitimate and when the user is not authorized. |
| deleteProfile() | `BehavAuthPrompt.DeletionCallback` callback | This deletes the profile of the user and also deletes any data and knowledge extracted until that point. | 

The constants for the BehavAuth class are shown below

| Name | Value | Description|
|-------------|------------|-------------| 
| `HIGH_ACCURACY` | 0 | This mode provides the highest accuracy and requires that the user uses and interacts with the phone. Actions such as leaving the phone on the table will immediately trigger an authenticationFailed() event.|
| `USER_EXPERIENCE` | 1 | This mode is similar to previous one but is more tolerant. Actions such as leaving the phone on the table will trigger an authenticationFailed() event after 8 seconds.|
| `PASSIVE` | 2 | This mode will provide events only when an imposter has taken the device. Actions such as leaving the phone on the table will not trigger any event. |

The interface for the BehavAuthPrompt.RegistrationCallback class is provided below.

| Method name | Parameters | Description|
|-------------|------------|-------------| 
| onRegistrationSuccess |No parameters  | Called when the registration was successfully completed. In case it was interrupted, th registration will continue from the previous point.|
| onRegistrationFailure | No parameters | Called when the registration failed. |

The interface for the BehavAuthPrompt.AuthenticationCallback class is provided below.

| Method name | Parameters | Description|
|-------------|------------|-------------| 
| onAuthenticationSucceeded | No parameters | Called when the authentication was successfully completed. |
| onAuthenticationFailed | No parameters  | Called when the authentication failed. This means that either a non authorised user has access to the device or the user is not using the device. |


The interface for the BehavAuthPrompt.DeletionCallback class is provided below.

| Method name | Parameters | Description|
|-------------|------------|-------------| 
| onDeletionSuccess | No parameters | Called when the user was deleted successfully.|
| onDeletionFailure | No parameters | Called when the user was not deleted successfully. |



